'use strict';

var mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    bcrypt = require('bcrypt'),
    User = mongoose.model('User');

exports.register = function (req, res) {
    return new Promise((resolve, reject) => {
        var newUser = new User(req.body);
        newUser.hash_password = bcrypt.hashSync(req.body.password, 10);
        newUser.save().then((user) => {
            resolve(user);
            }
        ).catch((error) => {
            reject(error);
        });

    })
};

exports.sign_in = function (req, res) {
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) throw err;
        if (!user) {
            res.status(401).json({message: 'Authentication failed. User not found.'});
        }
        else if (user) {
            if (!user.comparePassword(req.body.password)) {
                res.status(401).json({
                    token: 'Authentication failed. Wrong password.'
                });
            }
            else {
                return res.json({
                    token: jwt.sign({
                        email: user.email, fullName: user.fullName, _id: user._id
                    }, 'RESTFULAPIs')
                });
            }
        }
    })
};

exports.loginRequired = function (req, res, next) {
    if (req.user) {
        next();
    } else {
        console.log('nasol');
        return res.status(401).json({
            message: 'Unauthorized user!'
        })
    }
};