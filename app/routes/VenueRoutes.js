var mongoose = require('mongoose'),
    Venue = mongoose.model('Venue');

module.exports = function (app , loginNeeded) {

    app.get('/venues', (req, res) => {
        Venue.find({}).then(function(venues){
            res.send(venues);
        })
    });

    app.post('/venue',loginNeeded,(req,res)=>{
           var newVenue = new Venue(req.body);
           newVenue.save().then((venue)=>{
               res.json(venue);
           }).catch((err)=>{
               res.status(400).send({
                   message:err
               })
           })
    });

    app.post('/deleteVenue',(req,res)=>{
        Venue.remove({ _id: req.body.id }).then(()=>{
            res.json({message:'Venue sters cu id-ul ' + req.body.id })
        }).catch((err)=>{
            res.status(400).send({message:err})
        })
    });

    app.get('/venue/:id',loginNeeded,(req,res)=>{
        console.log(req.params.id);
       Venue.findOne({_id:req.params.id}).then(venue=>{
           res.json(venue);
       }).catch((err)=>{
           res.status(400).send({
               message:err
           });
       })
    })
};