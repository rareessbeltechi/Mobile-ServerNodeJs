const Sequelize = require('sequelize');
const sequelize = new Sequelize('sqlite:/Development/SQLite/FO');
/**
 * models
 */
const VenueModel = require('../models/Venue.js');

const Venue = VenueModel(sequelize, Sequelize);

sequelize.sync().then(() => {
    console.log('Database & tables created!');
});

module.exports ={
  Venue
};
