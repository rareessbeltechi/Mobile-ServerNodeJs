const
    express = require('express'),
    app = express(),
    port = 3131,
    mongoose = require('mongoose'),
    connectionDB = mongoose.connect('mongodb://localhost/mongoose_basics',{
        useNewUrlParser : true
    }).then(()=>{
       console.log('open db')
       }
    ).catch((error)=>{
        console.log(error);
    }),
    User = require('../models/User.js'),
    Venue = require('../models/Venue.js'),
    jsonwebtoken = require('jsonwebtoken'),
    bodyParser = require('body-parser'),
    userHandlers = require('../controller/UserController.js'),
// dataBase = require('../database/DataBase.js'),
venueRoutes = require('../routes/VenueRoutes.js');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.raw());

app.use(function (req, res, next) {
    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
        jsonwebtoken.verify(req.headers.authorization.split(' ')[1], 'RESTFULAPIs', function (err, decode) {
            if (err)
                req.user = undefined;
            req.user = decode;
            next();
        })
    }
    else {
        req.user = undefined;
        next();
    }
});

app.post('/tasks', userHandlers.loginRequired, function (req, res) {

});

app.post('/auth/register', function (req, res) {
    userHandlers.register(req, res).then((user)=>{
        res.json(user);
       }
    ).catch((error)=>{
        res.status(400).send({
            message: error
        });
    });
});

app.post('/auth/sign_in', function (req, res) {
    userHandlers.sign_in(req, res);
});
venueRoutes(app ,userHandlers.loginRequired);

app.listen(port, () => {
    console.log("The server is on ! ");
});