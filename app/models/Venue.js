'use strict';

// module.exports = function (sequelize, type) {
//     return sequelize.define('Venue', {
//         name: {
//             type: type.STRING
//         },
//         description: {
//             type: type.STRING
//         },
//         urlImg: {
//             type: type.STRING
//         }
//     })
// };

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/*
Venue Schema
 */
var VenueSchema = new Schema({
    name: {
        type: String
    },
    description: {
        type: String
    },
    urlImg: {
        type: String
    }
});

mongoose.model('Venue', VenueSchema);